package com.pld.jtys.controller;

import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.service.MessageService;
import com.pld.jtys.vo.UpdateMessageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author hc
 * @Date 2023/2/13 9:10
 */
@RestController
@RequestMapping("/msg")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @GetMapping("/getMessage")
    private Result getMessage() {
        UserDTO user = UserHolder.getUser();
        return messageService.getMessage(user.getId(),user.getType());
    }

    // 未读->已读 已读->回收站
    // action "1":未读->已读 "2":已读->回收站 "3" 回收站->已读
    @PostMapping("/updateMessage")
    private Result updateMessage(@RequestBody UpdateMessageVo updateMessageVo) {
        UserDTO user = UserHolder.getUser();
        return messageService.updateMessage(updateMessageVo.getMsgIds(),user.getId(),user.getType(), updateMessageVo.getAction());
    }

    @DeleteMapping("/delMessage")
    private Result delMessage(Integer[] msgIds) {
        UserDTO user = UserHolder.getUser();
        return messageService.delMessage(msgIds,user.getId(),user.getType());
    }

    @GetMapping("/countMsg")
    private Result countMsg() {
        UserDTO user = UserHolder.getUser();
        return messageService.countMsg(user.getId(),user.getType());
    }

}
