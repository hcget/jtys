package com.pld.jtys.controller;

import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.AppRequest;
import com.pld.jtys.entity.Customer;
import com.pld.jtys.entity.Doctor;
import com.pld.jtys.entity.Healthinfo;
import com.pld.jtys.service.AppRequestService;
import com.pld.jtys.service.CustomerService;
import com.pld.jtys.service.DoctorService;
import com.pld.jtys.service.HealthinfoService;
import com.pld.jtys.vo.AppInfoVo;
import com.pld.jtys.vo.CusAccountVo;
import com.pld.jtys.vo.CustomerViewVo;
import com.pld.jtys.vo.DocAccountVo;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

/**
 * @Author hc
 * @Date 2023/1/4 12:43
 */
@RestController
@RequestMapping("/cus")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private HealthinfoService healthinfoService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private AppRequestService appRequestService;

    //查看用户个人信息
    @GetMapping("/customerview")
    public Result customerview(){
        UserDTO user = UserHolder.getUser();
        if (!"1".equals(user.getType())) {
            return Result.errorResult(401,"请登录病人用户");
        }
        Customer customer = customerService.getCustomer(user.getId());
        Healthinfo healthinfo = healthinfoService.getCusHealth(customer.getHiId());
        return Result.okResult(new CustomerViewVo(customer,healthinfo));
    }

    // 查看签约医生信息
    @GetMapping("/doctorview")
    public Result doctorview(){
        return customerService.doctorview();
    }

    // 修改用户账户信息
    @PutMapping("/updatemes")
    public Result updatepswd(@RequestBody CusAccountVo cusAccountVo){
        return customerService.updatemes(cusAccountVo);
    }

    // 查看所有医生
    @GetMapping("/getAllDoctor")
    public Result getAllDoctor(@RequestParam @Nullable String docName){
        UserDTO user = UserHolder.getUser();
        return doctorService.getAllDoctor(docName,user.getId());
    }

    // 查看医生详情
    @GetMapping("/getDoctorDetail/{id}")
    public Result getDoctorDetail(@PathVariable("id") Integer id){
        return doctorService.getDoctorDetail(id);
    }

    // 申请医生(根据医生id)
    @PostMapping("/applyDoctor/{id}")
    public Result applyDoctor(@PathVariable("id") Integer id) {
        Integer cusId = UserHolder.getUser().getId();
        return  customerService.applyDoctor(cusId,id);
    }

    // 查询已经签约并且已经同意预约的医生
    @GetMapping("/getApp")
    public Result getAppByCus() {
        Integer cusId = UserHolder.getUser().getId();
        return appRequestService.getAppByCus(cusId);
    }

    // 申请就诊预约
    @PostMapping("/appVisit")
    public Result appVisit(@RequestBody AppInfoVo appInfoVo) {
        UserDTO user = UserHolder.getUser();
        return customerService.appVisit(user,appInfoVo);
    }

    // 修改个人信息
    @PostMapping("/updateInfo")
    public Result updateInfo(@RequestBody Customer customer) {
        return customerService.addCus(customer);
    }
}
