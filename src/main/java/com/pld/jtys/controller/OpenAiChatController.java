package com.pld.jtys.controller;

import cn.hutool.core.text.UnicodeUtil;
import com.alibaba.fastjson.JSON;
import com.pld.jtys.ai.Answer;
import com.pld.jtys.ai.Choices;
import com.pld.jtys.ai.OpenAi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Author hc
 * @Date 2023/2/16 9:28
 */
@RestController
public class OpenAiChatController {

    @Value("${openai.secretKey}")
    private String secretKey;
    @Autowired
    private OpenAi openAi;
    @Autowired
    private WebClient webClient;

    @GetMapping(value = "/openai/request")
    public Flux<String> request(HttpServletResponse response, @RequestParam String question) {
        Flux<String> submit = submit(response, question);
        return submit;
    }


    @PostMapping(value = "/openai/request")
    public Flux<String> request(HttpServletResponse response, @RequestBody OpenAi openAi) {
        Flux<String> submit = submit(response, openAi.getPrompt());
        return submit;
    }


    private Flux<String> submit(HttpServletResponse response, @RequestParam String question) {
        response.setContentType("text/event-stream;charset=utf-8");
        openAi.setPrompt(question);
        WebClient.ResponseSpec authorization = webClient.post()
                .uri("https://api.openai.com/v1/completions")
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + secretKey)
                .bodyValue(JSON.toJSONString(openAi))
                .retrieve();
        Flux<String> stringFlux = authorization.bodyToFlux(String.class);
        return stringFlux.mapNotNull(string -> {
            if (string.contains("DONE")) {
                return "Answer Done!";
            }
            Answer answer = JSON.parseObject(string, Answer.class);
            StringBuilder stringBuilder = new StringBuilder();
            List<Choices> choices = answer.getChoices();
            choices.forEach(choice -> {
                stringBuilder.append(UnicodeUtil.toString(choice.getText()));
            });
            StringBuilder s = new StringBuilder(stringBuilder.toString());
            if (s.toString().contains("\n")) {
                s = new StringBuilder("<br/>");
            }
            return s.toString().replace(" ", "&ensp;").replaceAll("\"","");
        }).cache();
    }

}
