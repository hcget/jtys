package com.pld.jtys.controller;

import cn.hutool.core.util.StrUtil;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.Doctor;
import com.pld.jtys.entity.Healthinfo;
import com.pld.jtys.entity.Treattime;
import com.pld.jtys.service.*;
import com.pld.jtys.vo.DocAccountVo;
import io.micrometer.core.lang.Nullable;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @Author hc
 * @Date 2022/12/23 18:46
 */
@RestController
@RequestMapping("/doc")
public class DoctorController {
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private AppRequestService appRequestService;
    @Autowired
    private HealthinfoService healthinfoService;
    @Autowired
    private DocCusService docCusService;
    @Autowired
    private TreattimeService treattimeService;

    // 医生个人信息
    @GetMapping("/getInfo")
    public Result getInfo(){
        return doctorService.getInfo();
    }

    // 获取所有签约病人
    @GetMapping("/browse")
    public Result getAllCustomer(@Nullable @RequestParam String name){
        Integer docId = UserHolder.getUser().getId();
        return customerService.getAllCustomer(docId,name);
    }

    // 查询某个病人的详细信息
    @GetMapping("/customerview/{id}")
    public Result getCustomerView(@PathVariable("id") Integer id){
        return doctorService.getCustomerView(id);
    }

    // 修改医生账户密码
    @PutMapping("/updatemes")
    public Result updatemes(@RequestBody DocAccountVo docAccountVo){
        return doctorService.updatemes(docAccountVo);
    }

    // 查看预约信息
    @GetMapping("/checkorder")
    public Result checkorder(@Nullable @RequestParam String cusName){
        Integer docId = UserHolder.getUser().getId();
        return appRequestService.checkorder(docId,cusName);
    }

    // 接受或者拒绝就诊预约
    @PostMapping("/accept/{appReqId}")
    public Result acceptOrReject(@PathVariable Integer appReqId){
        return appRequestService.accept(appReqId);
    }

    // 添加健康履历
    @PostMapping("/addhealth/{cId}")
    public Result addhealth(@PathVariable("cId") Integer cId, @RequestBody Healthinfo healthinfo){
        return healthinfoService.addhealth(cId,healthinfo);
    }

    // 查看病人签约申请
    @GetMapping("/getApply")
    public Result getApply(){
        Integer docId = UserHolder.getUser().getId();
        return doctorService.getApply(docId);
    }

    // 同意或者拒绝病人签约申请
    @PostMapping("/acceptApply/{duNum}")
    public Result acceptApply(@PathVariable Integer duNum, @RequestParam Integer action){
        return docCusService.acceptApply(duNum,action);
    }

    // 获取所有已经接收了的预约就诊信息
    @GetMapping("/getApp")
    public Result getApp() {
        Integer docId = UserHolder.getUser().getId();
        return appRequestService.getApp(docId);
    }

    // 结束就诊
    @PostMapping("/endInquiry/{name}")
    public Result endInquiry(@PathVariable String name) {
        UserDTO user = UserHolder.getUser();
        if (StrUtil.isEmpty(name)) {
            throw new GeneralException("请选择对象");
        }
        return appRequestService.endInquiry(user, name);
    }


    // 转诊
    @PostMapping("/transfer/{appReqId}/{docId}")
    public Result transfer(@PathVariable Integer appReqId, @PathVariable Integer docId) {
        UserDTO user = UserHolder.getUser();
        if (Objects.equals(docId, user.getId())) {
            throw new GeneralException("不能转给自己");
        }
        return appRequestService.transfer(appReqId,docId,user);
    }

    // 修改个人信息
    @PostMapping("/updateInfo")
    public Result updateInfo(@RequestBody Doctor doctor) {
        return doctorService.addDoc(doctor);
    }

    // 获取病人伤病履历
    @GetMapping("/getillness/{cId}")
    public Result getillness(@PathVariable Integer cId) {
        UserDTO user = UserHolder.getUser();
        return treattimeService.getillness(user,cId);
    }

    //添加/伤病履历
    @PostMapping("/addOrSaveIllness")
    public Result addillness(@RequestBody Treattime treattime) {
        return treattimeService.addOrSaveIllness(treattime);
    }

    // 删除
    @DeleteMapping("/del/{treatId}")
    public Result delillness(@PathVariable Integer treatId) {
        return treattimeService.delillness(treatId);
    }
}
