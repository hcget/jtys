package com.pld.jtys.controller;

import cn.hutool.core.util.StrUtil;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.common.util.RedisCache;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.service.AdministratorService;
import com.pld.jtys.service.CustomerService;
import com.pld.jtys.service.DoctorService;
import com.pld.jtys.vo.LoginVo;
import com.pld.jtys.vo.RegisterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.pld.jtys.common.domain.SystemConstants.LOGIN_KEY;


/**
 * @Author hc
 * @Date 2022/12/23 8:30
 */
@RestController
public class LoginController {
    @Autowired
    private AdministratorService administratorService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private RedisCache redisCache;

    // 登录
    @PostMapping("/login")
    public Result login(@RequestBody LoginVo loginVo){
        if ("0".equals(loginVo.getType())) {
            return administratorService.adminLogin(loginVo);
        } else if ("1".equals(loginVo.getType())) {
            return customerService.cusLogin(loginVo);
        } else if ("2".equals(loginVo.getType())) {
            return doctorService.docLogin(loginVo);
        }
        return null;
    }

    // 退出登录
    @PostMapping("/logout")
    public Result logout(HttpServletRequest request) {
        String token = request.getHeader("token");
        redisCache.deleteObject(LOGIN_KEY + token);
        return Result.okResult();
    }

    // 注册
    @PostMapping("/register")
    public Result register(@RequestBody RegisterVo registerVo) {
        if (StrUtil.isEmpty(registerVo.getUserName())) {
            throw new GeneralException(AppHttpCodeEnum.USERNAME_NOT_NULL);
        }
        if (StrUtil.isEmpty(registerVo.getPhonenum())) {
            throw new GeneralException(AppHttpCodeEnum.PHONENUMBER_NOTEXIST);
        }
        if ("0".equals(registerVo.getType())) {
            administratorService.register(registerVo);
        } else if ("1".equals(registerVo.getType())) {
            customerService.register(registerVo);
        } else if ("2".equals(registerVo.getType())) {
            doctorService.register(registerVo);
        }
        return null;
    }
}
