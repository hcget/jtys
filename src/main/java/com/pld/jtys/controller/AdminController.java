package com.pld.jtys.controller;

import cn.hutool.core.util.StrUtil;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.entity.Customer;
import com.pld.jtys.entity.Doctor;
import com.pld.jtys.service.AdministratorService;
import com.pld.jtys.service.CustomerService;
import com.pld.jtys.service.DoctorService;
import com.pld.jtys.vo.AdminAccountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

/**
 * @Author hc
 * @Date 2023/2/15 9:54
 */
@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdministratorService administratorService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private CustomerService customerService;

    // 查看医生
    @GetMapping("/getDocs")
    public Result getDocs(@RequestParam @Nullable String docName) {
        return doctorService.getAllDoctor(docName, null);
    }

    // 添加医生
    @PostMapping("/addDoc")
    public Result addDoc(@RequestBody Doctor doctor) {
        if (StrUtil.isEmpty(doctor.getDocName())) {
            throw new GeneralException(AppHttpCodeEnum.USERNAME_NOT_NULL);
        }
        return doctorService.addDoc(doctor);
    }

    // 删除医生
    @DeleteMapping("/delDoc/{id}")
    public Result delDoc(@PathVariable Integer id){
        return doctorService.delDoc(id);
    }

    // 查询病人
    @GetMapping("/getCus")
    public Result getCus(@RequestParam @Nullable String cusName) {
        return customerService.getCus(cusName);
    }

    // 增加病人用户
    @PostMapping("/addCus")
    public Result addCus(@RequestBody Customer customer) {
        if (StrUtil.isEmpty(customer.getCusName())) {
            throw new GeneralException(AppHttpCodeEnum.USERNAME_NOT_NULL);
        }
        return customerService.addCus(customer);
    }

    // 删除病人用户
    @DeleteMapping("/delCus/{id}")
    public Result delCus(@PathVariable Integer id) {
        return customerService.delCus(id);
    }

    // 管理员修改密码
    @PostMapping("/updatepswd")
    public Result updateAdmin(@RequestBody AdminAccountVo adminAccountVo) {
        if ("".equals(adminAccountVo.getNewPswd())) {
            throw new GeneralException(AppHttpCodeEnum.PASSWORD_NOT_NULL);
        }
        return administratorService.updateAdmin(adminAccountVo);
    }
}
