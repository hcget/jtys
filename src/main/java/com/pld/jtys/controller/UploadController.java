package com.pld.jtys.controller;

import com.pld.jtys.common.domain.Result;
import com.pld.jtys.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author hc
 * @Date 2023/2/15 10:49
 */
@RestController
public class UploadController {
    @Autowired
    private UploadService uploadService;

    @PostMapping("/upload")
    public Result uploadImg(@RequestPart("img") MultipartFile img){
        return uploadService.uploadImg(img);
    }
}
