package com.pld.jtys.ai;

import lombok.Data;

@Data
public class Choices {
    private String text;
    private Integer index;
    private String logprobs;
    private String finish_reason;
}
