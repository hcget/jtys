package com.pld.jtys.dto;

import com.pld.jtys.entity.Customer;
import com.pld.jtys.entity.Healthinfo;
import com.pld.jtys.entity.Medcinerecords;
import com.pld.jtys.entity.Treattime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author hc
 * @Date 2023/1/3 16:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CusDetailsDto {
    private Customer customer;
    private Healthinfo healthinfo;
    private List<Treattime> treats;
    private List<Medcinerecords> med;
}
