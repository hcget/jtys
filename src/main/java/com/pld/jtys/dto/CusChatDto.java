package com.pld.jtys.dto;

import lombok.Data;

import java.util.Date;

/**
 * @Author hc
 * @Date 2023/2/14 15:00
 */
@Data
public class CusChatDto {
    private Integer cusId;

    private String cusName;

    private String cusPhonenum;

    private String cusAddress;

    private String cusOld;

    private String cusIdentity;

    private Date cusBirthday;

    private String cusNation;

    private String cusGender;

    // 在线状态(0不在线，1在线)
    private Integer state;

}
