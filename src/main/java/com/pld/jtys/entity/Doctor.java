package com.pld.jtys.entity;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Doctor)表实体类
 *
 * @author hc
 * @since 2022-12-23 18:26:57
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("doctor")
public class Doctor implements Serializable {
    private static final long serialVersionUID = -45072761927753243L;
    @TableId
    private Integer docId;

    
    private String docPswd;
    
    private String docName;
    
    private String docHospital;
    
    private String docDepartment;
    
    private String docSecdepartment;
    
    private String docPhonenum;
    
    private String docOld;
    
    private String docGender;
    
    private String docIdentification;
    
    private String docDegree;
    
    private String docRemark;
    
    private String docSpeciality;

    private String avatar;
}

