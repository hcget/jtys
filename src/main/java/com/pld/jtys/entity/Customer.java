package com.pld.jtys.entity;

import java.util.Date;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Customer)表实体类
 *
 * @since 2022-12-21 22:47:24
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("customer")
public class Customer implements Serializable {
    private static final long serialVersionUID = -57629209795695150L;
    @TableId
    private Integer cusId;

    
    private String cusPswd;
    
    private String cusName;
    
    private String cusPhonenum;
    
    private String cusAddress;
    
    private String cusOld;
    
    private String cusIdentity;
    
    private Date cusBirthday;
    
    private String cusNation;

    private String cusGender;
    
    private Integer hiId;

    private String avatar;
}

