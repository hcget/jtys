package com.pld.jtys.entity;

import java.util.Date;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Medicine)表实体类
 *
 * @since 2022-12-22 13:27:48
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("medicine")
public class Medicine implements Serializable {
    private static final long serialVersionUID = -57839754274792321L;
    @TableId
    private Integer medId;

    
    private String medName;

    private Date medTime;
    
    private String medAmount;
    
    private String medMethod;
    
    private String medPeriod;
    
    private String medUsage;
}

