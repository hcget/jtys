package com.pld.jtys.entity;

import java.util.Date;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (WarningInfo)表实体类
 *
 * @since 2022-12-22 13:29:19
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("warning_info")
public class WarningInfo implements Serializable {
    private static final long serialVersionUID = 236687366398748023L;
    @TableId
    private Integer warnId;

    
    private Integer docId;
    
    private Integer cusId;
    
    private Date warnTime;
    
    private String warnText;
}

