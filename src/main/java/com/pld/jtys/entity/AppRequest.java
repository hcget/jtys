package com.pld.jtys.entity;

import java.util.Date;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (AppRequest)表实体类
 *
 * @author hc
 * @since 2023-01-04 10:59:02
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("app_request")
public class AppRequest implements Serializable {
    private static final long serialVersionUID = 862718785685616380L;
    @TableId
    private Integer appReqId;

    
    private Integer docId;
    
    private Integer cusId;
    
    private Date appReqTime;
    
    private String appReqText;
    
    private Integer appStatement;
    
    private Integer appGrade;
    
    private String cusName;
    
    private String appReqIllness;

    private String cusGender;

    // 预约申请状态 0未接收，1接收
    private Integer status;
}

