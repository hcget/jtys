package com.pld.jtys.entity;

import java.util.Date;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Medcinerecords)表实体类
 *
 * @since 2022-12-22 13:29:43
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("medcinerecords")
public class Medcinerecords implements Serializable {
    private static final long serialVersionUID = -56847265015975751L;
    @TableId
    private Integer medId;

    
    private String medName;
    
    private Integer cusId;
    
    private Date medTime;
    
    private Float medAmount;
    
    private String medMethod;
    
    private String medPeirod;
}

