package com.pld.jtys.entity;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Administrator)表实体类
 *
 * @since 2022-12-21 22:40:10
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("administrator")
public class Administrator implements Serializable {
    private static final long serialVersionUID = -48211330747388432L;
    @TableId
    private Integer adminId;

    
    private String adminName;
    
    private String adminPswd;
    // 手机号
    private String adminPhonenum;

    private String avatar;
}

