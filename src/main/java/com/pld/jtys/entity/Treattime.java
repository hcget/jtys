package com.pld.jtys.entity;

import java.util.Date;

import java.io.Serializable;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Treattime)表实体类
 *
 * @since 2022-12-21 22:57:39
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("treattime")
public class Treattime implements Serializable {
    private static final long serialVersionUID = -36727846779606036L;
    @TableId
    private Integer treatId;

    private Integer docId;

    private Integer cusId;
    
    private String docName;
    
    private String cusName;
    
    private Date treatTime;
    
    private String treatAppend;
    // 备注
    private String info;
    // 伤病信息
    private String operation;
}

