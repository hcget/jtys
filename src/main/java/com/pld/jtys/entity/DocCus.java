package com.pld.jtys.entity;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (DocCus)表实体类
 *
 * @since 2022-12-22 13:30:18
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("doc_cus")
public class DocCus implements Serializable {
    private static final long serialVersionUID = 477467098964160922L;
    @TableId
    private Integer duNum;

    
    private Integer docId;
    
    private Integer cusId;

    // 医生是否已经同意签约申请：1同意，0拒绝
    private Integer status;
}

