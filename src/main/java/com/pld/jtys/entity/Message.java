package com.pld.jtys.entity;

import java.util.Date;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Message)表实体类
 *
 * @author hc
 * @since 2023-01-10 09:52:02
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("message")
public class Message implements Serializable {
    private static final long serialVersionUID = 338774012895138627L;
    @TableId
    private Integer id;

    //内容
    private String content;
    //状态(0未读,1已读)
    private String status;
    //消息时间
    private Date msgTime;
    //用户类型("0"管理员"1"病人"2"医生)
    private String type;
    //用户id
    private Integer userId;
}

