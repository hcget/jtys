package com.pld.jtys.entity;


import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Healthinfo)表实体类
 *
 * @since 2022-12-21 22:45:16
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("healthinfo")
public class Healthinfo implements Serializable {
    private static final long serialVersionUID = 864974877347565863L;
    @TableId
    private Integer hiId;

    
    private String hiHeight;
    
    private String hiWeight;
    
    private Float hiPressure;
    
    private Float hiGlucose;
    
    private Float hiLipids;
    
    private String hiAge;
    
    private Float hiBonedensity;
    
    private Float hiSao2;

    private Float hiPulse;

    private Float hiLowPressure;

    private String hiState;

    private Date hiTime;

    private String hiEvaluate;
}

