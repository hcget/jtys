package com.pld.jtys;

import cn.hutool.core.text.UnicodeUtil;
import com.alibaba.fastjson.JSON;
import com.pld.jtys.ai.Answer;
import com.pld.jtys.ai.Choices;
import com.pld.jtys.ai.OpenAi;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Date 2022/12/22 13:32
 */
@SpringBootApplication
@MapperScan("com.pld.jtys.mapper")
@ConfigurationPropertiesScan(basePackages = "com.pld.jtys.ai")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
