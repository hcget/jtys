package com.pld.jtys.vo;

import com.pld.jtys.entity.Doctor;
import lombok.Data;

/**
 * @Author hc
 * @Date 2023/2/16 15:37
 */
@Data
public class CusGetDocsVo extends Doctor {
    private Integer isMyDoc;
}
