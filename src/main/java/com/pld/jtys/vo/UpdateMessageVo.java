package com.pld.jtys.vo;

import lombok.Data;

/**
 * @Author hc
 * @Date 2023/2/14 16:07
 */
@Data
public class UpdateMessageVo {
    private String action;
    private Integer[] msgIds;
}
