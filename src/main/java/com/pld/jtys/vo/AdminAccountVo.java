package com.pld.jtys.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author hc
 * @Date 2023/2/20 8:50
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminAccountVo {
    // 旧密码
    private String oldPswd;
    // 新密码
    private String newPswd;
}
