package com.pld.jtys.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author hc
 * @Date 2023/1/4 14:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CusAccountVo {
    private String cusName;
    // 旧密码
    private String oldPswd;
    // 新密码
    private String newPswd;
}
