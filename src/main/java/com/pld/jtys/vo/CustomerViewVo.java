package com.pld.jtys.vo;

import com.pld.jtys.entity.Customer;
import com.pld.jtys.entity.Healthinfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author hc
 * @Date 2023/1/4 13:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerViewVo {
    private Customer customer;
    private Healthinfo healthinfo;
}
