package com.pld.jtys.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author hc
 * @Date 2023/1/3 17:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocAccountVo {
    private String docName;
    // 旧密码
    private String oldPswd;
    // 新密码
    private String newPswd;
}
