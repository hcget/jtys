package com.pld.jtys.vo;

import lombok.Data;

/**
 * @Author hc
 * @Date 2023/2/14 10:54
 */
@Data
public class RegisterVo {
    private String userName;
    private String phonenum;
    private String password;
    // 用户类型
    private String type;
}
