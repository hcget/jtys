package com.pld.jtys.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Author hc
 * @Date 2023/2/22 11:44
 */
@Data
public class AppInfoVo {
    private Integer docId;
    private Date appReqTime;
    private String appReqIllness;
}
