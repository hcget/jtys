package com.pld.jtys.vo;

import com.pld.jtys.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 签约申请VO
 * @Author hc
 *
 * @Date 2023/1/13 8:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplyDetailVo {
    private Customer customer;
    // 关联表id
    private Integer duNum;
}
