package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.WarningInfo;


/**
 * (WarningInfo)表数据库访问层
 *
 * @since 2022-12-22 13:29:19
 */
public interface WarningInfoMapper extends BaseMapper<WarningInfo> {

}

