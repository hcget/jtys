package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.Customer;


/**
 * (Customer)表数据库访问层
 *
 * @since 2022-12-21 22:47:24
 */
public interface CustomerMapper extends BaseMapper<Customer> {

}

