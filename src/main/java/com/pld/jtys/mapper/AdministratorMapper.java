package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.Administrator;


/**
 * (Administrator)表数据库访问层
 *
 * @since 2022-12-21 22:40:11
 */
public interface AdministratorMapper extends BaseMapper<Administrator> {

}

