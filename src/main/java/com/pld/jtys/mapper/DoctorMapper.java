package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.Doctor;


/**
 * (Doctor)表数据库访问层
 *
 * @author hc
 * @since 2022-12-23 18:27:01
 */
public interface DoctorMapper extends BaseMapper<Doctor> {

}

