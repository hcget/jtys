package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.Message;


/**
 * (Message)表数据库访问层
 *
 * @author hc
 * @since 2023-01-10 09:52:02
 */
public interface MessageMapper extends BaseMapper<Message> {

}

