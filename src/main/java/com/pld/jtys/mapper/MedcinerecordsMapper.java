package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.Medcinerecords;


/**
 * (Medcinerecords)表数据库访问层
 *
 * @since 2022-12-22 13:29:43
 */
public interface MedcinerecordsMapper extends BaseMapper<Medcinerecords> {

}

