package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.Treattime;


/**
 * (Treattime)表数据库访问层
 *
 * @since 2022-12-21 22:57:39
 */
public interface TreattimeMapper extends BaseMapper<Treattime> {

}

