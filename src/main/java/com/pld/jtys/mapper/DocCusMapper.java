package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.DocCus;


/**
 * (DocCus)表数据库访问层
 *
 * @since 2022-12-22 13:30:18
 */
public interface DocCusMapper extends BaseMapper<DocCus> {

}

