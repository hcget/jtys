package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.AppRequest;


/**
 * (AppRequest)表数据库访问层
 *
 * @author hc
 * @since 2023-01-04 10:59:02
 */
public interface AppRequestMapper extends BaseMapper<AppRequest> {

}

