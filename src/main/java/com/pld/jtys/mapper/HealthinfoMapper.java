package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.Healthinfo;


/**
 * (Healthinfo)表数据库访问层
 *
 * @since 2022-12-21 22:45:16
 */
public interface HealthinfoMapper extends BaseMapper<Healthinfo> {

}

