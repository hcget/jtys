package com.pld.jtys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pld.jtys.entity.Medicine;


/**
 * (Medicine)表数据库访问层
 *
 * @since 2022-12-22 13:27:51
 */
public interface MedicineMapper extends BaseMapper<Medicine> {

}

