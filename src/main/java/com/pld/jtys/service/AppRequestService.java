package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.AppRequest;
import com.pld.jtys.vo.AppInfoVo;


/**
 * (AppRequest)表服务接口
 *
 * @author hc
 * @since 2023-01-04 10:59:02
 */
public interface AppRequestService extends IService<AppRequest> {

    Result checkorder(Integer docId, String cusName);


    // 获取所有已经接收了的预约就诊信息
    Result getApp(Integer docId);

    Result endInquiry(UserDTO user, String name);

    Result getAppByCus(Integer cusId);

    // 接收就诊预约
    Result accept(Integer appReqId);

    // 转诊
    Result transfer(Integer appReqId, Integer docId, UserDTO user);
}

