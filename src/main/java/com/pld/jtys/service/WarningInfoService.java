package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.entity.WarningInfo;


/**
 * (WarningInfo)表服务接口
 *
 * @since 2022-12-22 13:29:19
 */
public interface WarningInfoService extends IService<WarningInfo> {

}

