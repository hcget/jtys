package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.entity.Message;


/**
 * (Message)表服务接口
 *
 * @author hc
 * @since 2023-01-10 09:52:02
 */
public interface MessageService extends IService<Message> {

    Result getMessage(Integer id, String type);

    Result updateMessage(Integer[] msgIds, Integer id, String type, String action);

    Result delMessage(Integer[] msgIds, Integer id, String type);

    // 新增消息
    boolean newMsg(Integer userId, String userType, String content);

    Result countMsg(Integer id, String type);
}

