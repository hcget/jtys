package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.entity.Medicine;


/**
 * (Medicine)表服务接口
 *
 * @since 2022-12-22 13:27:50
 */
public interface MedicineService extends IService<Medicine> {

}

