package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.entity.Medcinerecords;

import java.util.List;


/**
 * (Medcinerecords)表服务接口
 *
 * @since 2022-12-22 13:29:43
 */
public interface MedcinerecordsService extends IService<Medcinerecords> {

    // 查询病人用药记录
    List<Medcinerecords> getCusRecords(Integer cusId);
}

