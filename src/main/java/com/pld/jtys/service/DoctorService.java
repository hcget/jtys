package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.entity.Doctor;
import com.pld.jtys.vo.DocAccountVo;
import com.pld.jtys.vo.LoginVo;
import com.pld.jtys.vo.RegisterVo;


/**
 * (Doctor)表服务接口
 *
 * @author hc
 * @since 2022-12-23 18:26:58
 */
public interface DoctorService extends IService<Doctor> {

    Result docLogin(LoginVo loginVo);

    Result getInfo();

    Result getCustomerView(Integer id);

    Result updatemes(DocAccountVo docAccountVo);

    Result getAllDoctor(String docName, Integer cusId);

    Result getDoctorDetail(Integer id);

    Result getApply(Integer docId);

    void register(RegisterVo registerVo);

    Result addDoc(Doctor doctor);

    Result delDoc(Integer id);
}

