package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.entity.DocCus;


/**
 * (DocCus)表服务接口
 *
 * @since 2022-12-22 13:30:18
 */
public interface DocCusService extends IService<DocCus> {

    Result acceptApply(Integer duNum, Integer action);
}

