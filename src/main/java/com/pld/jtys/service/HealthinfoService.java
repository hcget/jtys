package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.entity.Healthinfo;

import java.util.List;


/**
 * (Healthinfo)表服务接口
 *
 * @since 2022-12-21 22:45:16
 */
public interface HealthinfoService extends IService<Healthinfo> {

    // 根据id查健康信息
    Healthinfo getCusHealth(Integer id);

    Result addhealth(Integer cId, Healthinfo healthinfo);
}

