package com.pld.jtys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.entity.Message;
import com.pld.jtys.mapper.MessageMapper;
import com.pld.jtys.service.MessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * (Message)表服务实现类
 *
 * @author hc
 * @since 2023-01-10 09:52:02
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {

    @Override
    public Result getMessage(Integer userId, String type) {
        LambdaQueryWrapper<Message> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Message::getType, type);
        queryWrapper.eq(Message::getUserId,userId);
        List<Message> messageList = this.list(queryWrapper);

        return Result.okResult(messageList);
    }

    @Override
    @Transactional
    public Result updateMessage(Integer[] msgIds, Integer userId, String type, String action) {
        if (msgIds.length == 0) {
            throw new GeneralException(AppHttpCodeEnum.MESSAGE_ISBLANK);
        }
        LambdaUpdateWrapper<Message> updateWrapper = new LambdaUpdateWrapper<>();
        if ("1".equals(action)) {
            this.update().setSql("status = '1'").in("id",Arrays.asList(msgIds)).eq("user_id",userId).eq("type",type).update();
        }
        if ("2".equals(action)) {
            this.update().setSql("status = '2'").in("id",Arrays.asList(msgIds)).eq("user_id",userId).eq("type",type).update();
        }
        if ("3".equals(action)) {
            this.update().setSql("status = '3'").in("id",Arrays.asList(msgIds)).eq("user_id",userId).eq("type",type).update();
        }

        return Result.okResult();
    }

    @Override
    public Result delMessage(Integer[] msgIds, Integer userId, String type) {
        if (msgIds.length == 0) {
            throw new GeneralException(AppHttpCodeEnum.MESSAGE_ISBLANK);
        }
        LambdaQueryWrapper<Message> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Message::getType,type).eq(Message::getUserId,userId).in(Message::getId,Arrays.asList(msgIds));
        this.remove(queryWrapper);

        return Result.okResult();
    }

    @Override
    public boolean newMsg(Integer userId, String userType, String content) {
        Message message = new Message();
        message.setContent(content);
        message.setType(userType);
        message.setUserId(userId);
        message.setMsgTime(new Date());
        return this.save(message);
    }

    @Override
    public Result countMsg(Integer userId, String type) {
        LambdaQueryWrapper<Message> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Message::getType, type);
        queryWrapper.eq(Message::getUserId,userId);
        queryWrapper.eq(Message::getStatus,"0");
        int count = this.count(queryWrapper);

        return Result.okResult(count);
    }
}

