package com.pld.jtys.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.common.util.LoginUtil;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.AppRequest;
import com.pld.jtys.entity.Customer;
import com.pld.jtys.entity.DocCus;
import com.pld.jtys.entity.Doctor;
import com.pld.jtys.mapper.CustomerMapper;
import com.pld.jtys.service.*;
import com.pld.jtys.vo.AppInfoVo;
import com.pld.jtys.vo.CusAccountVo;
import com.pld.jtys.vo.LoginVo;
import com.pld.jtys.vo.RegisterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (Customer)表服务实现类
 *
 * @since 2022-12-21 22:47:24
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements CustomerService {
    @Autowired
    private LoginUtil loginUtil;
    @Autowired
    private DocCusService docCusService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private AppRequestService appRequestService;
    @Autowired
    private MessageService messageService;

    @Override
    public Result cusLogin(LoginVo loginVo) {
        // 校验用户名
        Customer customer = this.getOne(new LambdaQueryWrapper<Customer>()
                .eq(Customer::getCusName, loginVo.getUsername()));
        if (customer == null) {
            return Result.errorResult(AppHttpCodeEnum.REQUIRE_USERNAME);
        }
        // 校验密码
        if (!loginVo.getPassword().equals(customer.getCusPswd())) {
            return Result.errorResult(AppHttpCodeEnum.PASSWORD_ERROR);
        }
        UserDTO userDTO = new UserDTO(customer.getCusId(),customer.getCusName(),loginVo.getType());
        String token = loginUtil.loginToRedis(userDTO);
        return Result.okResult(token);

    }

    @Override
    public Result getAllCustomer(Integer docId, String name) {
        List<DocCus> docCusList = docCusService.list(new LambdaQueryWrapper<DocCus>().eq(DocCus::getDocId, docId).eq(DocCus::getStatus,1));
        List<Integer> cusIds = docCusList.stream().map(DocCus::getCusId).collect(Collectors.toList());
        LambdaQueryWrapper<Customer> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StrUtil.isNotEmpty(name), Customer::getCusName, name);
        queryWrapper.in(Customer::getCusId,cusIds);
        List<Customer> customers = this.list(queryWrapper);
        return Result.okResult(customers);
    }

    @Override
    public Customer getCustomer(Integer id) {
        Customer customer = this.getById(id);
        return customer;
    }

    @Override
    public Result doctorview() {
        Integer cusId = UserHolder.getUser().getId();
        List<DocCus> docCusList = docCusService.list(new LambdaQueryWrapper<DocCus>().eq(DocCus::getCusId, cusId));
        List<Integer> docIds = docCusList.stream().map(DocCus::getDocId).collect(Collectors.toList());
        List<Doctor> doctors = doctorService.listByIds(docIds);
        return Result.okResult(doctors);
    }

    @Override
    public Result updatemes(CusAccountVo cusAccountVo) {
        Integer cusId = UserHolder.getUser().getId();
        Customer customer  = this.getById(cusId);
        String cusPswd = customer.getCusPswd();
        // 验证旧密码是否正确
        if (!cusAccountVo.getOldPswd().equals(cusPswd)) {
            return Result.errorResult(401,"密码不正确");
        }
        if (cusAccountVo.getOldPswd().equals(cusAccountVo.getNewPswd())) {
            return Result.errorResult(401,"两次密码不允许相同");
        }

        customer.setCusPswd(cusAccountVo.getNewPswd());
        this.updateById(customer);
        return Result.okResult();
    }

    @Override
    public Result applyDoctor(Integer cusId, Integer docId) {
        // 排除已签约的医生
        LambdaQueryWrapper<DocCus> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(DocCus::getCusId).eq(DocCus::getDocId,docId);
        List<DocCus> docCusList = docCusService.list(queryWrapper);
        docCusList.forEach(docCus -> {
            if (Objects.equals(docCus.getCusId(), cusId)) {
                throw new GeneralException(AppHttpCodeEnum.ALREADY_APPLIED);
            }
        });

        // 添加申请
        DocCus docCus = new DocCus();
        docCus.setCusId(cusId);
        docCus.setDocId(docId);
        docCusService.save(docCus);
        return Result.okResult();
    }

    @Override
    public void register(RegisterVo registerVo) {
        String userName = registerVo.getUserName();
        int count = this.count(new LambdaQueryWrapper<Customer>().eq(Customer::getCusName, userName));
        if (count > 0) {
            throw new GeneralException(AppHttpCodeEnum.USERNAME_EXIST);
        }
        int count1 = this.count(new LambdaQueryWrapper<Customer>().eq(Customer::getCusPhonenum, registerVo.getPhonenum()));
        if (count1 > 0) {
            throw new GeneralException(AppHttpCodeEnum.PHONENUMBER_EXIST);
        }
        Customer customer = new Customer();
        customer.setCusPswd(registerVo.getPassword());
        customer.setCusName(registerVo.getUserName());
        customer.setCusPhonenum(registerVo.getPhonenum());

        this.save(customer);
    }

    @Override
    public Result getCus(String cusName) {
        LambdaQueryWrapper<Customer> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StrUtil.isNotEmpty(cusName),Customer::getCusName,cusName);
        List<Customer> customers = this.list(queryWrapper);

        return Result.okResult(customers);
    }

    @Override
    public Result addCus(Customer customer) {
        this.saveOrUpdate(customer);

        return Result.okResult();
    }

    @Override
    public Result delCus(Integer id) {
        boolean remove = this.removeById(id);
        if (!remove) {
            throw new GeneralException(AppHttpCodeEnum.SYSTEM_ERROR);
        }
        return Result.okResult();
    }

    @Override
    @Transactional
    public Result appVisit(UserDTO user, AppInfoVo appInfoVo) {
        AppRequest appRequest = BeanUtil.copyProperties(appInfoVo, AppRequest.class);
        Customer customer = this.getById(user.getId());
        int count = appRequestService.count(new LambdaQueryWrapper<AppRequest>().eq(AppRequest::getDocId, appInfoVo.getDocId()).eq(AppRequest::getCusId, customer.getCusId()));
        if (count > 0) {
            throw new GeneralException("已经预约过此医生");
        }
        appRequest.setCusId(customer.getCusId());
        appRequest.setCusName(customer.getCusName());
        appRequest.setCusGender(customer.getCusGender());
        appRequestService.save(appRequest);

        // 给医生发消息
        messageService.newMsg(appInfoVo.getDocId(),"2","您有新的预约就诊信息，请查看");

        return Result.okResult();
    }

}

