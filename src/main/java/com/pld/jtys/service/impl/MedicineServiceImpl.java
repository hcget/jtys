package com.pld.jtys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.entity.Medicine;
import com.pld.jtys.service.MedicineService;
import com.pld.jtys.mapper.MedicineMapper;
import org.springframework.stereotype.Service;

/**
 * (Medicine)表服务实现类
 *
 * @since 2022-12-22 13:27:50
 */
@Service
public class MedicineServiceImpl extends ServiceImpl<MedicineMapper, Medicine> implements MedicineService {

}

