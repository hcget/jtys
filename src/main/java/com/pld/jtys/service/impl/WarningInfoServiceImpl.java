package com.pld.jtys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.entity.WarningInfo;
import com.pld.jtys.mapper.WarningInfoMapper;
import com.pld.jtys.service.WarningInfoService;
import org.springframework.stereotype.Service;

/**
 * (WarningInfo)表服务实现类
 *
 * @since 2022-12-22 13:29:19
 */
@Service
public class WarningInfoServiceImpl extends ServiceImpl<WarningInfoMapper, WarningInfo> implements WarningInfoService {

}

