package com.pld.jtys.service.impl;

import cn.hutool.core.date.DateTime;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.service.UploadService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * @Author hc
 * @Date 2023/2/15 11:02
 */
@Service
public class UploadServiceImpl implements UploadService {

    @Value("${aliyun.oss.file.endpoint}")
    private String endpoint;
    @Value("${aliyun.oss.file.accessKeyId}")
    private String accessKeyId;
    @Value("${aliyun.oss.file.accessKeySecret}")
    private String accessKeySecret;
    @Value("${aliyun.oss.file.bucketName}")
    private String bucketName;      // 对象存储bucket

    @Override
    public Result uploadImg(MultipartFile img) {
        //获取原始文件名
        String fileName = img.getOriginalFilename();
        // 判断文件格式
        if (!(fileName.endsWith(".png")||fileName.endsWith(".jpg"))){
            throw new GeneralException(AppHttpCodeEnum.FILE_TYPE_ERROR);
        }
        // 按照日期分类
        String datePath = new DateTime().toString("yyyy/MM/dd");
        fileName = datePath + "/" + fileName;
        // 创建OSS实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        //获取上传文件输入流
        InputStream inputStream = null;
        try {
            inputStream = img.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        ossClient.putObject(bucketName, fileName, inputStream);
        // 获取url
        // 设置签名URL过期时间，单位为毫秒。
        Date expiration = new Date(new Date().getTime() + 86400000L * 100);   // 100天有效时间
        // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
        URL url = ossClient.generatePresignedUrl(bucketName, fileName, expiration);
        // 关闭OSSClient。
        ossClient.shutdown();

        // 返回url
        return Result.okResult(url.toString());
    }
}
