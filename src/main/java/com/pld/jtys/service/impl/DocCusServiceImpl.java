package com.pld.jtys.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.entity.DocCus;
import com.pld.jtys.mapper.DocCusMapper;
import com.pld.jtys.service.DocCusService;
import org.springframework.stereotype.Service;

/**
 * (DocCus)表服务实现类
 *
 * @since 2022-12-22 13:30:18
 */
@Service
public class DocCusServiceImpl extends ServiceImpl<DocCusMapper, DocCus> implements DocCusService {

    @Override
    public Result acceptApply(Integer duNum, Integer action) {
        LambdaUpdateWrapper<DocCus> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(DocCus::getDuNum,duNum);
        updateWrapper.set(DocCus::getStatus,action);
        this.update(updateWrapper);
        return Result.okResult();
    }
}

