package com.pld.jtys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.entity.Customer;
import com.pld.jtys.entity.Healthinfo;
import com.pld.jtys.mapper.HealthinfoMapper;
import com.pld.jtys.service.CustomerService;
import com.pld.jtys.service.HealthinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * (Healthinfo)表服务实现类
 *
 * @since 2022-12-21 22:45:16
 */
@Service
public class HealthinfoServiceImpl extends ServiceImpl<HealthinfoMapper, Healthinfo> implements HealthinfoService {

    @Autowired
    private CustomerService customerService;

    @Override
    public Healthinfo getCusHealth(Integer id) {
        LambdaQueryWrapper<Healthinfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Healthinfo::getHiId,id);
        Healthinfo healthinfo = this.getOne(queryWrapper);
        return healthinfo;
    }

    @Override
    @Transactional
    public Result addhealth(Integer cId, Healthinfo healthinfo) {
        this.save(healthinfo);

        // 病人healthinfo信息
        LambdaUpdateWrapper<Customer> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(Customer::getCusId,cId);
        updateWrapper.set(Customer::getHiId,healthinfo.getHiId());
        customerService.update(updateWrapper);
        return Result.okResult();
    }
}

