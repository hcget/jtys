package com.pld.jtys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.common.util.LoginUtil;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.Administrator;
import com.pld.jtys.entity.Customer;
import com.pld.jtys.entity.Doctor;
import com.pld.jtys.mapper.AdministratorMapper;
import com.pld.jtys.service.AdministratorService;
import com.pld.jtys.vo.AdminAccountVo;
import com.pld.jtys.vo.LoginVo;
import com.pld.jtys.vo.RegisterVo;
import com.pld.jtys.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * (Administrator)表服务实现类
 *
 * @since 2022-12-21 22:40:10
 */
@Service
public class AdministratorServiceImpl extends ServiceImpl<AdministratorMapper, Administrator> implements AdministratorService {

    @Autowired
    private LoginUtil loginUtil;

    @Override
    public Result adminLogin(LoginVo loginVo) {
        // 校验用户名
        Administrator administrator = this.getOne(new LambdaQueryWrapper<Administrator>()
                .eq(Administrator::getAdminName, loginVo.getUsername()));
        if (administrator == null) {
            return Result.errorResult(AppHttpCodeEnum.REQUIRE_USERNAME);
        }
        // 校验密码
        if (!loginVo.getPassword().equals(administrator.getAdminPswd())) {
            return Result.errorResult(AppHttpCodeEnum.PASSWORD_ERROR);
        }

        UserDTO userDTO = new UserDTO(administrator.getAdminId(),administrator.getAdminName(),loginVo.getType());
        String token = loginUtil.loginToRedis(userDTO);
        return Result.okResult(token);
    }

    @Override
    public void register(RegisterVo registerVo) {
        String userName = registerVo.getUserName();
        int count = this.count(new LambdaQueryWrapper<Administrator>().eq(Administrator::getAdminName, userName));
        if (count > 0) {
            throw new GeneralException(AppHttpCodeEnum.USERNAME_EXIST);
        }
        int count1 = this.count(new LambdaQueryWrapper<Administrator>().eq(Administrator::getAdminPhonenum, registerVo.getPhonenum()));
        if (count1 > 0) {
            throw new GeneralException(AppHttpCodeEnum.PHONENUMBER_EXIST);
        }
        Administrator administrator = new Administrator();
        administrator.setAdminPswd(registerVo.getPassword());
        administrator.setAdminName(registerVo.getUserName());
        administrator.setAdminPhonenum(registerVo.getPhonenum());

        this.save(administrator);
    }

    @Override
    public Result updateAdmin(AdminAccountVo adminAccountVo) {
        Integer cusId = UserHolder.getUser().getId();
        Administrator administrator  = this.getById(cusId);
        String adminPswd = administrator.getAdminPswd();
        // 验证旧密码是否正确
        if (!adminAccountVo.getOldPswd().equals(adminPswd)) {
            return Result.errorResult(401,"密码不正确");
        }
        if (adminAccountVo.getOldPswd().equals(adminAccountVo.getNewPswd())) {
            return Result.errorResult(401,"两次密码不允许相同");
        }

        administrator.setAdminPswd(adminAccountVo.getNewPswd());
        this.updateById(administrator);
        return Result.okResult();

    }
}

