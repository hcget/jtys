package com.pld.jtys.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.common.util.LoginUtil;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.dto.CusDetailsDto;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.*;
import com.pld.jtys.mapper.DoctorMapper;
import com.pld.jtys.service.*;
import com.pld.jtys.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * (Doctor)表服务实现类
 *
 * @author hc
 * @since 2022-12-23 18:27:00
 */
@Service
public class DoctorServiceImpl extends ServiceImpl<DoctorMapper, Doctor> implements DoctorService {
    @Autowired
    private DocCusService docCusService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private MedcinerecordsService medcinerecordsService;
    @Autowired
    private TreattimeService treattimeService;
    @Autowired
    private HealthinfoService healthinfoService;

    @Autowired
    private LoginUtil loginUtil;

    @Override
    public Result docLogin(LoginVo loginVo) {
        // 校验用户名
        Doctor doctor = this.getOne(new LambdaQueryWrapper<Doctor>()
                .eq(Doctor::getDocName, loginVo.getUsername()));
        if (doctor == null) {
            return Result.errorResult(AppHttpCodeEnum.REQUIRE_USERNAME);
        }
        // 校验密码
        if (!loginVo.getPassword().equals(doctor.getDocPswd())) {
            return Result.errorResult(AppHttpCodeEnum.PASSWORD_ERROR);
        }

        UserDTO userDTO = new UserDTO(doctor.getDocId(),doctor.getDocName(),loginVo.getType());
        String token = loginUtil.loginToRedis(userDTO);
        return Result.okResult(token);

    }

    @Override
    public Result getInfo() {
        Integer id = UserHolder.getUser().getId();
        Doctor doctor = this.getById(id);
        return Result.okResult(doctor);
    }

    @Override
    public Result getCustomerView(Integer cusId) {
        // 查询用户
        Customer customer = customerService.getById(cusId);
        // 查询健康评估信息
        Healthinfo healthinfo = healthinfoService.getCusHealth(customer.getHiId());
        // 查询健康履历(治疗信息)
        List<Treattime> treattimeList = treattimeService.getCusTreatTime(cusId);
        // 查询用药记录
        List<Medcinerecords> medcinerecordsList = medcinerecordsService.getCusRecords(cusId);
        // 封装返回
        CusDetailsDto cusDetailsDto = new CusDetailsDto(customer, healthinfo, treattimeList, medcinerecordsList);
        return Result.okResult(cusDetailsDto);
    }

    @Override
    public Result updatemes(DocAccountVo docAccountVo) {
        Integer docId = UserHolder.getUser().getId();
        Doctor doctor = this.getById(docId);
        String docPswd = doctor.getDocPswd();
        // 验证旧密码是否正确
        if (!docAccountVo.getOldPswd().equals(docPswd)) {
            return Result.errorResult(401,"密码不正确");
        }
        if (docAccountVo.getOldPswd().equals(docAccountVo.getNewPswd())) {
            return Result.errorResult(401,"两次密码不允许相同");
        }

        doctor.setDocPswd(docAccountVo.getNewPswd());
        this.updateById(doctor);
        return Result.okResult();
    }

    @Override
    public Result getAllDoctor(String docName, Integer cusId) {
        LambdaQueryWrapper<Doctor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StrUtil.isNotEmpty(docName),Doctor::getDocName,docName);
        List<Doctor> doctors = this.list(queryWrapper);
        List<CusGetDocsVo> collect = doctors.stream().map(doctor -> {
            CusGetDocsVo cusGetDocsVo = new CusGetDocsVo();
            BeanUtil.copyProperties(doctor, cusGetDocsVo);
            cusGetDocsVo.setIsMyDoc(0);
            if (cusId == null) {
                return cusGetDocsVo;
            }
            LambdaQueryWrapper<DocCus> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(DocCus::getCusId,cusId).eq(DocCus::getDocId,doctor.getDocId()).eq(DocCus::getStatus,1);
            int count = docCusService.count(wrapper);
            if (count>0) {
                cusGetDocsVo.setIsMyDoc(1);
            }
            return cusGetDocsVo;
        }).collect(Collectors.toList());

        return Result.okResult(collect);
    }

    @Override
    public Result getDoctorDetail(Integer id) {
        Doctor doctor = this.getById(id);
        return Result.okResult(doctor);
    }

    @Override
    public Result getApply(Integer docId) {
        LambdaQueryWrapper<DocCus> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DocCus::getDocId,docId).eq(DocCus::getStatus,0);
        List<DocCus> docCusList = docCusService.list(queryWrapper);
        if (docCusList == null || docCusList.size() == 0) {
            return Result.okResult(null);
        }
        List<ApplyDetailVo> applyDetailVoList = docCusList.stream().map(docCus -> {
            Customer customer = customerService.getById(docCus.getCusId());
            ApplyDetailVo applyDetailVo = null;
            if (customer != null) {
                applyDetailVo = new ApplyDetailVo(customer, docCus.getDuNum());
            }
            return applyDetailVo;
        }).collect(Collectors.toList());

        // List<Integer> cusIds = docCusList.stream().map(DocCus::getCusId).collect(Collectors.toList());
        //
        // List<Customer> customerList = customerService.list(new LambdaQueryWrapper<Customer>().in(Customer::getCusId, cusIds));
        return Result.okResult(applyDetailVoList);
    }

    @Override
    public void register(RegisterVo registerVo) {
        String userName = registerVo.getUserName();
        int count = this.count(new LambdaQueryWrapper<Doctor>().eq(Doctor::getDocName, userName));
        if (count > 0) {
            throw new GeneralException(AppHttpCodeEnum.USERNAME_EXIST);
        }
        int count1 = this.count(new LambdaQueryWrapper<Doctor>().eq(Doctor::getDocPhonenum, registerVo.getPhonenum()));
        if (count1 > 0) {
            throw new GeneralException(AppHttpCodeEnum.PHONENUMBER_EXIST);
        }
        Doctor doctor = new Doctor();
        doctor.setDocPswd(registerVo.getPassword());
        doctor.setDocName(registerVo.getUserName());
        doctor.setDocPhonenum(registerVo.getPhonenum());

        this.save(doctor);

    }

    @Override
    public Result addDoc(Doctor doctor) {
        this.saveOrUpdate(doctor);
        return Result.okResult();
    }

    @Override
    public Result delDoc(Integer id) {
        boolean remove = this.removeById(id);
        if (!remove) {
            throw new GeneralException(AppHttpCodeEnum.SYSTEM_ERROR);
        }
        return Result.okResult();
    }


}

