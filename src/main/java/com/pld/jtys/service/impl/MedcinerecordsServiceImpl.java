package com.pld.jtys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.entity.Medcinerecords;
import com.pld.jtys.mapper.MedcinerecordsMapper;
import com.pld.jtys.service.MedcinerecordsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Medcinerecords)表服务实现类
 *
 * @since 2022-12-22 13:29:43
 */
@Service
public class MedcinerecordsServiceImpl extends ServiceImpl<MedcinerecordsMapper, Medcinerecords> implements MedcinerecordsService {

    @Override
    public List<Medcinerecords> getCusRecords(Integer cusId) {
        List<Medcinerecords> list = this.list(new LambdaQueryWrapper<Medcinerecords>().eq(Medcinerecords::getCusId, cusId));

        return list;
    }
}

