package com.pld.jtys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.Doctor;
import com.pld.jtys.entity.Treattime;
import com.pld.jtys.mapper.TreattimeMapper;
import com.pld.jtys.service.DoctorService;
import com.pld.jtys.service.TreattimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Treattime)表服务实现类
 *
 * @since 2022-12-21 22:57:39
 */
@Service
public class TreattimeServiceImpl extends ServiceImpl<TreattimeMapper, Treattime> implements TreattimeService {
    @Autowired
    private DoctorService doctorService;

    @Override
    public List<Treattime> getCusTreatTime(Integer cusId) {

        List<Treattime> list = this.list(new LambdaQueryWrapper<Treattime>().eq(Treattime::getCusId, cusId));
        return list;
    }

    @Override
    public Result getillness(UserDTO user, Integer cId) {
        LambdaQueryWrapper<Treattime> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Treattime::getCusId,cId);
        List<Treattime> list = this.list(queryWrapper);
        return Result.okResult(list);
    }

    @Override
    public Result addOrSaveIllness(Treattime treattime) {
        Integer docId = UserHolder.getUser().getId();
        treattime.setDocId(docId);
        Doctor doctor = doctorService.getById(docId);
        treattime.setDocName(doctor.getDocName());
        this.saveOrUpdate(treattime);
        return Result.okResult();
    }

    @Override
    public Result delillness(Integer treatId) {
        this.removeById(treatId);
        return Result.okResult();
    }
}

