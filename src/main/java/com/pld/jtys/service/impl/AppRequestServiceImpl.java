package com.pld.jtys.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.exception.GeneralException;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.AppRequest;
import com.pld.jtys.entity.Customer;
import com.pld.jtys.entity.Doctor;
import com.pld.jtys.mapper.AppRequestMapper;
import com.pld.jtys.service.AppRequestService;
import com.pld.jtys.service.CustomerService;
import com.pld.jtys.service.DoctorService;
import com.pld.jtys.service.MessageService;
import com.pld.jtys.vo.AppInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * (AppRequest)表服务实现类
 *
 * @author hc
 * @since 2023-01-04 10:59:02
 */
@Service
public class AppRequestServiceImpl extends ServiceImpl<AppRequestMapper, AppRequest> implements AppRequestService {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private MessageService messageService;

    @Override
    public Result checkorder(Integer docId, String cusName) {
        // 只查看未同意的预约就诊信息
        LambdaQueryWrapper<AppRequest> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppRequest::getDocId, docId).eq(AppRequest::getStatus,0);
        queryWrapper.like(StrUtil.isNotEmpty(cusName),AppRequest::getCusName,cusName);
        List<AppRequest> appRequests = this.list(queryWrapper);
        return Result.okResult(appRequests);
    }


    @Override
    public Result getApp(Integer docId) {
        LambdaQueryWrapper<AppRequest> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppRequest::getDocId,docId).eq(AppRequest::getStatus,1);
        List<AppRequest> appRequestList = this.list(queryWrapper);
        List<Integer> cusIds = appRequestList.stream().map(AppRequest::getCusId).collect(Collectors.toList());
        if (cusIds.size() <= 0) {
            return Result.okResult(null);
        }
        List<Customer> customers = customerService.listByIds(cusIds);
        return Result.okResult(customers);
    }

    @Override
    public Result endInquiry(UserDTO user, String name) {
        LambdaQueryWrapper<AppRequest> queryWrapper = new LambdaQueryWrapper<>();
        // 病人
        if ("1".equals(user.getType())) {
            Doctor doctor = doctorService.getOne(new LambdaQueryWrapper<Doctor>().eq(Doctor::getDocName, name));
            if (doctor == null) {
                throw new GeneralException("不存在此医生");
            }
            queryWrapper.eq(AppRequest::getCusId,user.getId()).eq(AppRequest::getDocId,doctor.getDocId());
        }

        // 医生
        if ("2".equals(user.getType())) {
            queryWrapper.eq(AppRequest::getDocId,user.getId()).eq(AppRequest::getCusName,name);
        }
        AppRequest one = this.getOne(queryWrapper);
        if (one == null) {
            throw new GeneralException("就诊不存在");
        }
        one.setStatus(3);
        this.updateById(one);
        return Result.okResult();
    }

    @Override
    public Result getAppByCus(Integer cusId) {
        LambdaQueryWrapper<AppRequest> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AppRequest::getCusId,cusId).eq(AppRequest::getStatus,1);
        List<AppRequest> appRequestList = this.list(queryWrapper);
        List<Integer> docIds = appRequestList.stream().map(AppRequest::getDocId).collect(Collectors.toList());
        if (docIds.size() <= 0) {
            return Result.okResult(null);
        }
        List<Doctor> doctors = doctorService.listByIds(docIds);
        return Result.okResult(doctors);
    }

    @Override
    public Result accept(Integer appReqId) {
        LambdaUpdateWrapper<AppRequest> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(AppRequest::getAppReqId,appReqId).set(AppRequest::getStatus,1);
        this.update(updateWrapper);
        return Result.okResult();
    }

    @Override
    @Transactional
    public Result transfer(Integer appReqId, Integer docId, UserDTO user) {
        // 查询医生是否存在
        Doctor doctor = doctorService.getById(docId);
        if (doctor == null) {
            throw new GeneralException("医生不存在");
        }

        LambdaUpdateWrapper<AppRequest> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(AppRequest::getAppReqId,appReqId).set(AppRequest::getDocId,docId);
        this.update(updateWrapper);
        // 消息提示
        boolean flag = messageService.newMsg(docId,user.getType(),"您有其他医生转诊的病人，请注意查看");
        if (!flag) {
            throw new GeneralException("失败");
        }

        return Result.okResult();
    }

}

