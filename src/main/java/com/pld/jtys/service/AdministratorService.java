package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.entity.Administrator;
import com.pld.jtys.vo.AdminAccountVo;
import com.pld.jtys.vo.LoginVo;
import com.pld.jtys.vo.RegisterVo;


/**
 * (Administrator)表服务接口
 *
 * @since 2022-12-21 22:40:10
 */
public interface AdministratorService extends IService<Administrator> {

    Result adminLogin(LoginVo loginVo);

    void register(RegisterVo registerVo);

    Result updateAdmin(AdminAccountVo adminAccountVo);
}

