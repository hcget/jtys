package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.Treattime;

import java.util.List;


/**
 * (Treattime)表服务接口
 *
 * @since 2022-12-21 22:57:39
 */
public interface TreattimeService extends IService<Treattime> {

    // 根据病人id查询所有治疗信息
    List<Treattime> getCusTreatTime(Integer cusId);

    // 获取病人伤病履历
    Result getillness(UserDTO user, Integer cId);

    // 新增伤病履历
    Result addOrSaveIllness(Treattime treattime);

    Result delillness(Integer treatId);
}

