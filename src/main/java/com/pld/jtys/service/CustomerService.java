package com.pld.jtys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.entity.Customer;
import com.pld.jtys.vo.AppInfoVo;
import com.pld.jtys.vo.CusAccountVo;
import com.pld.jtys.vo.LoginVo;
import com.pld.jtys.vo.RegisterVo;


/**
 * (Customer)表服务接口
 *
 * @since 2022-12-21 22:47:24
 */
public interface CustomerService extends IService<Customer> {

    Result cusLogin(LoginVo loginVo);


    Result getAllCustomer(Integer docId, String name);

    Customer getCustomer(Integer id);

    Result doctorview();

    Result updatemes(CusAccountVo cusAccountVo);

    Result applyDoctor(Integer cusId, Integer docId);

    void register(RegisterVo registerVo);

    // 管理员查询病人用户
    Result getCus(String cusName);

    // 管理员添加病人用户
    Result addCus(Customer customer);

    // 管理员删除病人用户
    Result delCus(Integer id);

    // 申请就诊预约
    Result appVisit(UserDTO userDTO, AppInfoVo appInfoVo);
}

