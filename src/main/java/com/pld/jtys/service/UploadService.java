package com.pld.jtys.service;

import com.pld.jtys.common.domain.Result;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author hc
 * @Date 2023/2/15 10:59
 */
public interface UploadService {
    Result uploadImg(MultipartFile img);
}
