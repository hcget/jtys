package com.pld.jtys.common.domain;


public enum AppHttpCodeEnum {
    // 成功
    SUCCESS(200,"操作成功"),
    // 登录
    NEED_LOGIN(401,"需要登录后操作"),
    NO_OPERATOR_AUTH(403,"无权限操作"),
    ALREADY_APPLIED(403,"无权限操作"),
    MESSAGE_ISBLANK(406,"消息为空"),
    SYSTEM_ERROR(500,"出现错误"),
    USERNAME_EXIST(501,"用户名已存在"),
    PHONENUMBER_EXIST(502,"手机号已存在"),
    EMAIL_EXIST(503, "邮箱已存在"),
    REQUIRE_USERNAME(504, "用户名不存在"),
    LOGIN_ERROR(505,"用户名或密码错误"),
    USERNAME_NOT_NULL(506,"用户名不能为空"),
    PASSWORD_NOT_NULL(507,"密码不能为空"),
    FILE_TYPE_ERROR(507,"文件类型有误"),
    EMAIL_NOT_NULL(508,"邮箱不能为空"),
    PASSWORD_ERROR(509,"密码错误"),
    PHONENUMBER_NOTEXIST(508,"邮箱不能为空");
    final int code;
    final String msg;

    AppHttpCodeEnum(int code, String errorMessage){
        this.code = code;
        this.msg = errorMessage;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
