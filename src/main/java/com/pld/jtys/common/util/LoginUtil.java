package com.pld.jtys.common.util;

import com.pld.jtys.dto.UserDTO;
import com.pld.jtys.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

import static com.pld.jtys.common.domain.SystemConstants.*;

/**
 * @Author hc
 * @Date 2023/1/3 11:47
 */
@Component
public class LoginUtil {
    @Autowired
    private RedisCache redisCache;

    public String loginToRedis(UserDTO userDTO){
        // 生成token
        String jwt = JwtUtil.createJWT(userDTO.getId().toString());
        // 存入redis
        String key = LOGIN_KEY + jwt;
        redisCache.setCacheObject(key, userDTO,30, TimeUnit.MINUTES);
        return jwt;
    }



}
