package com.pld.jtys.common.exception;

import com.pld.jtys.common.domain.AppHttpCodeEnum;

/**
 * 自定义异常
 */
public class GeneralException extends RuntimeException {
    private int code;

    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public GeneralException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public GeneralException(String msg) {
        super(msg);
        this.code = 408;
        this.msg = msg;
    }

    public GeneralException(AppHttpCodeEnum httpCodeEnum) {
        super(httpCodeEnum.getMsg());
        this.code = httpCodeEnum.getCode();
        this.msg = httpCodeEnum.getMsg();
    }
}
