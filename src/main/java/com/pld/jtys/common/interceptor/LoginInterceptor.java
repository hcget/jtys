package com.pld.jtys.common.interceptor;

import com.alibaba.fastjson.JSON;
import com.pld.jtys.common.domain.AppHttpCodeEnum;
import com.pld.jtys.common.domain.Result;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.common.util.WebUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录拦截器
 * @Author hc
 * @Date 2023/1/3 12:05
 */
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 1.判断是否需要拦截(ThreadLocal中是否存在用户)
        if (UserHolder.getUser() == null){
            // 没有用户，登录过期
            //说明登录过期  提示重新登录
            Result result = Result.errorResult(AppHttpCodeEnum.NEED_LOGIN);
            WebUtils.renderString(response, JSON.toJSONString(result));
            return false;
        }
        // 有用户，放行
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // 移除用户，防止内存溢出
        UserHolder.removeUser();
    }


}
