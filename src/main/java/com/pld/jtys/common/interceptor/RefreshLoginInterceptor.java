package com.pld.jtys.common.interceptor;

import cn.hutool.core.util.StrUtil;
import com.pld.jtys.common.util.RedisCache;
import com.pld.jtys.common.util.UserHolder;
import com.pld.jtys.dto.UserDTO;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.concurrent.TimeUnit;

import static com.pld.jtys.common.domain.SystemConstants.LOGIN_KEY;

/**
 * @Author hc
 * @Date 2023/1/3 12:23
 */
public class RefreshLoginInterceptor implements HandlerInterceptor {

    private RedisCache redisCache;
    public RefreshLoginInterceptor(RedisCache redisCache) {
        this.redisCache = redisCache;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 1.请求头中获取token
        String token = request.getHeader("token");
        if (StrUtil.isEmpty(token)){
            // 没有携带token说明不需要token验证，直接放行
            return true;
        }
        // 2. 获取redis中的用户
        UserDTO userDTO = redisCache.getCacheObject(LOGIN_KEY + token);
        // 3.判断用户是否存在
        if (userDTO == null) {
            return true; //直接放行
        }
        // 5.存保存到TheadLocal
        UserHolder.saveUser(userDTO);
        // 6.刷新有效期
        redisCache.setCacheObject(LOGIN_KEY + token, userDTO,60, TimeUnit.MINUTES);
        // 7.放行
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // 移除用户，防止内存溢出
        UserHolder.removeUser();
    }
}
