package com.pld.jtys.config;

import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.pld.jtys.common.interceptor.LoginInterceptor;
import com.pld.jtys.common.interceptor.RefreshLoginInterceptor;
import com.pld.jtys.common.util.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Autowired
    private RedisCache redisCache;


    // 处理跨域问题
    // 不用addCorsMappings，如果配置了HandlerInterceptor拦截器，因为首先会被拦截器拦截，这个配置没用
    // @Override
    // public void addCorsMappings(CorsRegistry registry) {
    //     // 设置允许跨域的路径
    //     registry.addMapping("/**")
    //             // 设置允许跨域请求的域名
    //             .allowedOriginPatterns("*")
    //             // 是否允许cookie
    //             .allowCredentials(true)
    //             // 设置允许的请求方式
    //             .allowedMethods("GET", "POST", "DELETE", "PUT")
    //             // 设置允许的header属性
    //             .allowedHeaders("*")
    //             // 跨域允许时间
    //             .maxAge(3600);
    // }

    // @Bean//使用@Bean注入fastJsonHttpMessageConvert
    public HttpMessageConverter fastJsonHttpMessageConverters() {
        //1.需要定义一个Convert转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
        // 配置日期转化格式
        fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");

        SerializeConfig.globalInstance.put(Long.class, ToStringSerializer.instance);

        fastJsonConfig.setSerializeConfig(SerializeConfig.globalInstance);
        fastConverter.setFastJsonConfig(fastJsonConfig);
        HttpMessageConverter<?> converter = fastConverter;
        return converter;
    }

    // 修改默认数据转化(默认JSON转化)
    // @Override
    // public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    //     converters.add(fastJsonHttpMessageConverters());
    // }

    // 添加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 登录拦截器
        registry.addInterceptor(new LoginInterceptor())
                // .excludePathPatterns("/**").order(1);
                .excludePathPatterns("/login","/","websocket/**","/register","/openai/**","/upload/**").order(1);
        // 刷新拦截器 排序0
        registry.addInterceptor(new RefreshLoginInterceptor(redisCache)).order(0);

    }
}
