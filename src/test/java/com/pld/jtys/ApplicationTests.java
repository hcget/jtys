package com.pld.jtys;

import com.pld.jtys.entity.Administrator;
import com.pld.jtys.service.AdministratorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


/**
 * @Author hc
 * @Date 2022/12/22 13:47
 */

@SpringBootTest
public class ApplicationTests {
    @Autowired
    private AdministratorService administratorService;
    @Test
    public void test(){

        List<Administrator> list = administratorService.list();
        System.out.println(list);
    }
}
