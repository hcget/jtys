# 部署文档

项目前端地址：https://gitee.com/gaojie-123/my_doc

项目后端地址：https://gitee.com/hcget/jtys

项目部署基于Docker

## 1.后端部署

### docker安装

+ 安装yum-utils：

```sh
yum install -y yum-utils device-mapper-persistent-data lvm2
```

- 为yum源添加docker仓库位置：

```sh
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

+ 安装docker：

```sh
yum install docker-ce

systemctl start docker
```

### MySql安装

+ 下载MySQL5.7的docker镜像：

```sh
docker pull mysql:5.7
```

+ 启动容器

```sh
docker run -p 3306:3306 --name mysql \
-v /mydata/mysql/log:/var/log/mysql \
-v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=root  \
-d mysql:5.7
```

+ 进入容器，创建数据库

```sh
docker exec -it mysql /bin/bash

mysql -uroot -proot --default-character-set=utf8

create database jtys character set utf8
```

+ 将sql脚本拷贝至mysql容器内部：

```sh
docker cp /mydata/jtys.sql mysql:/
```

+ mysql容器导入sql脚本

```sh
use jtys;
source /jtys.sql;
```

### Redis安装

+ 下载Redis镜像

```sh
docker pull redis:7
```

+ 启动容器

```sh
docker run -p 6379:6379 --name redis \
-v /mydata/redis/data:/data \
-d redis:7 redis-server --appendonly yes
```

### Nginx安装

+ 下载镜像

```sh
docker pull nginx:1.22
```

+ 运行容器(拷贝配置文件)

```sh
docker run -p 80:80 --name nginx \
-v /mydata/nginx/html:/usr/share/nginx/html \
-v /mydata/nginx/logs:/var/log/nginx  \
-d nginx:1.22
```

+ 拷贝配置文件

```
docker container cp nginx:/etc/nginx /mydata/nginx/

mv nginx conf
```

+ 删除此容器

```sh
docker rm -f nginx
```

+ 启动nginx容器

```sh
docker run -p 80:80 --name nginx \
-v /mydata/nginx/html:/usr/share/nginx/html \
-v /mydata/nginx/logs:/var/log/nginx  \
-v /mydata/nginx/conf:/etc/nginx \
-d nginx:1.22
```

### Springboot项目部署

构建镜像并上传

+ 修改pom文件的docker.host属性

```sh
    <properties>
        <docker.host>http://xx.xx.xx.xx:2375</docker.host>
    </properties>
```

+ 打包maven项目

```sh
mvn package
```

+ 启动容器

  项目打包之后自动构建docker镜像

```sh
docker run -p 8080:8080 --name jtys \
--link mysql:db \
--link redis:redis \
-v /etc/localtime:/etc/localtime \
-v /mydata/app/jtys/logs:/var/logs \
-d jtys/jtys:1.0-SNAPSHOT
```

## 2.前端部署

+ 进入前端项目文件夹，打开控制台

```sh
npm install

npm run build
```

+ build之后会生成一个dist文件，将其上传至服务器/mydata/nginx路径下面，删除原来的html文件夹，将dist文件夹改为html：

```sh
rm -rf html

mv dist html
```

+ 重启nginx容器

```sh
docker restart nginx
```

注意nginx的配置，打开nginx.conf，简单的配置如下：

```
user  root;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;
}
```

